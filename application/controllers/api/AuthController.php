<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AuthModel');
    }

    public function doLogin(){
        if(!$this->input->post('username') || !$this->input->post('password'))
        {
            $response = array(
				'status'  => FALSE,
				'message' => 'Lengkapi Kredensial anda.');
			
            echo json_encode($response);
            exit;
        }

        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $data = $this->AuthModel->checkLogin($username, $password);

        if ($data) {
            $response = array(
				'status'  => TRUE,
				'message' => 'Login Berhasil');
			
			echo json_encode($response);
        }else{
                $response = array(
                    'status'  => FALSE,
                    'message' => 'Username dan Password Tidak Sesuai');
                
                echo json_encode($response);
            }
    }
}
