<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class AuthModel extends CI_Model {

    public function checkLogin($username,$password){ 

        $this->db->select('userID, name, email, username');
        $this->db->from('user');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        
        $query = $this->db->get();

        if($query->num_rows()==1)
        {
            $data    = $query->row();
                $content = array(
                    'username'        => $data->username,
                    'name'          => $data->name,
                    'email'     => $data->email,
                    'username'     => $data->username           
                );
            return $content;
        }else{
                return FALSE;
        }

    }
}

?>